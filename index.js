"use strict";
const Email = require('email-templates');
const helper = require('sendgrid').mail;
const sg = require('sendgrid')(process.env.SENDGRID_APIKEY);


class EmailProvider {
	constructor(config) {
		this._config = config;
		this._email = new Email({
			message: {
				from: this._config.email.from
			},
			send: this._config.email.force_send,
			transport: {
			    host: this._config.email.host,
			    port: this._config.email.port,
			    secure: false, // upgrade later with STARTTLS
			    auth: {
			        user: this._config.email.username,
			        pass: this._config.email.password
			    }
			},
			juiceResources: {
				preserveImportant: true,
				webResources: {
					relativeTo: this._config.email.style_dir
				}
			},
			views: {
				root: this._config.email.template_dir
			}
		});
	}

	send(opts) {
		this._email.render(opts.template, opts.locals)
		.then((html) => {
			console.log(html);
			var from_email = new helper.Email(this._config.email.from);
			var to_email = new helper.Email(opts.message.to);
			var subject = opts.message.subject;
			var content = new helper.Content('text/html', html);
			var mail = new helper.Mail(from_email, subject, to_email, content);

			var request = sg.emptyRequest({
			  method: 'POST',
			  path: '/v3/mail/send',
			  body: mail.toJSON(),
			});

			sg.API(request, function(error, response) {
			  console.log(response.statusCode);
			  console.log(response.body);
			  console.log(response.headers);
			});
		})
		.catch(console.error);
	}
}

module.exports = EmailProvider;