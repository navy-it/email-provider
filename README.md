Email Provider
===========================
A provider for Hazel that supports sending email using [email-templates](https://email-templates.js.org/#/) with EJS for the templating engine.

### Usage
```javascript
const EmailProvider = require('email-provider');

var emailProvider = new EmailProvider(hazelConfig);
emailProvider.send({
	template: 'ocean/register',
	message: {
		to: 'elon@spacex.com'
	},
	locals: {
		name: 'Elon',
		validation_url: "https://website.com/validate/23456543212345432345432"
	}
});
```

### Hazel Config Options
This provider requires the following extra items in your `config.default.js`

```javascript
{
	email: {
		from: 'you@domain.com',
		force_send: (true || false),
		style_dir: "path to email stylesheets",
		template_dir: "path to email templates"
	}
}
```

### Example Template
```ejs
<h3>Welcome <%name%>!</h3>
<p>
	Thank you for registering. Click the following link to finish registration: <a href="<%=validation_url%>"><%=validation_url%></a>
</p>
<h3>About the site</h3>
<p>
	Dolor sit amet.
</p>
```